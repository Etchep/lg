package fr.iban.lg.gui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import fr.iban.lg.Main;
import fr.iban.lg.objects.Role;

public class CompoGUI implements Listener {

	private Main main;

	public CompoGUI(Main main) {
		this.main = main;
	}

	public static Inventory inv = Bukkit.createInventory(null, 27, ChatColor.AQUA + "" + ChatColor.BOLD + "UHC > Scenarios > LG");

	@EventHandler
	public void onOpen(InventoryOpenEvent e) {
		// Vert : 5
		// Rouge : 14
		Inventory invent = e.getInventory();
		if(invent == inv) {
			if(main.compo.isEmpty()) {
				inv.setItem(0, main.itemBuilder.buildItem(Material.STAINED_CLAY, "", 1, 5, null, null, null));
				List<ItemStack> contentlist = new ArrayList<>();
				contentlist.add(main.itemBuilder.buildItem(Material.SKULL, Role.SIMPLE_VILLAGOIS.getName() , 1, 0, Arrays.asList("§aClic gauche : +1", "§cClic droit : -1"), null, null));
				main.compo.add(Role.SIMPLE_VILLAGOIS);
				main.compo.add(Role.SOEUR);
				for(Role role : Role.values()) {
					if(role != Role.SIMPLE_VILLAGOIS) {
						contentlist.add(main.itemBuilder.buildItem(Material.STAINED_CLAY, role.getName(), 1, 5, Arrays.asList("§a§lON"), null, null));
						main.compo.add(role);
					}
				}
				contentlist.forEach((item) -> {
					invent.addItem(item);
				});
			}
		}
	}

	@EventHandler
	public void onClick(final InventoryClickEvent e) {
		Inventory invent = e.getInventory();
		if(invent == inv) {
			ItemStack currentItem = e.getCurrentItem();
			if(currentItem.getType() == Material.SKULL) {
				if(e.getClick() == ClickType.LEFT) {
					if(currentItem.getAmount() == 64) return;
					//+1
					e.setCurrentItem(main.itemBuilder.buildItem(Material.SKULL, Role.SIMPLE_VILLAGOIS.getName() , currentItem.getAmount() + 1, 0, Arrays.asList("§aClic gauche : +1", "§cClic droit : -1"), null, null));
					main.compo.add(Role.SIMPLE_VILLAGOIS);
					return;
				}
				if(e.getClick() == ClickType.RIGHT) {
					if(currentItem.getAmount() == 1) return;
					//-1 
					e.setCurrentItem(main.itemBuilder.buildItem(Material.SKULL, Role.SIMPLE_VILLAGOIS.getName() , currentItem.getAmount() - 1, 0, Arrays.asList("§aClic gauche : +1", "§cClic droit : -1"), null, null));
					main.compo.remove(Role.SIMPLE_VILLAGOIS);
					return;

				}
			}
			if(currentItem.getType() == Material.STAINED_CLAY) {
				Role role = Role.find(currentItem.getItemMeta().getDisplayName());
				if(currentItem.getDurability() == 14) {
					// il est off -> on
					if(role == Role.SOEUR) {
						e.setCurrentItem(main.itemBuilder.buildItem(Material.STAINED_CLAY, role.getName(), 1, 14, Arrays.asList("§a§lON"), null, null));
						main.compo.add(role);
						main.compo.add(role);
					}else {
						e.setCurrentItem(main.itemBuilder.buildItem(Material.STAINED_CLAY, role.getName(), 1, 14, Arrays.asList("§a§lON"), null, null));
						main.compo.add(role);
					}
				}
				if(currentItem.getDurability() == 5) {
					// il est on -> off
					if(role == Role.SOEUR) {
						e.setCurrentItem(main.itemBuilder.buildItem(Material.STAINED_CLAY, role.getName(), 1, 5, Arrays.asList("§c§lOFF"), null, null));
						main.compo.remove(role);
						main.compo.remove(role);
					}else {
						e.setCurrentItem(main.itemBuilder.buildItem(Material.STAINED_CLAY, role.getName(), 1, 5, Arrays.asList("§c§lOFF"), null, null));
						main.compo.remove(role);
					}
				}
			}
			
		}
	}
}

