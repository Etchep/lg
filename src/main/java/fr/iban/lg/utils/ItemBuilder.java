package fr.iban.lg.utils;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemBuilder {
	
	public ItemStack buildItem(Material material, String name, int quantity, int damage, List<String> desc, List<Enchantment> enchant, List<Integer> enchantlvl){
		final ItemStack it = new ItemStack(material, quantity,  (short) damage);
		final ItemMeta itM = it.getItemMeta();
		if(name != null )itM.setDisplayName(name);
		if(desc != null) itM.setLore(desc);
		if(enchant != null) {
			if(enchantlvl != null) {
				if(enchant.size() == enchantlvl.size()) {
					for(int i = 0 ; i < enchant.size() ; i++) {
						itM.addEnchant(enchant.get(i), enchantlvl.get(i), true);
					}
				}
			}
		}
		it.setItemMeta(itM);
		return it;
	}

}
