package fr.iban.lg.utils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import fr.iban.lg.Main;
import fr.iban.lg.objects.Camp.CampType;
import fr.iban.lg.objects.Joueur;
import fr.iban.lg.objects.Role;
import lombok.Getter;
import lombok.Setter;

public class LgUtils {
	
	//Pour l'enfant :
	@Getter @Setter private Player modele;
	//Pour le renard
	@Getter @Setter private int renardCounter = 3;
	//Pour le salvateur:
	@Getter private List<UUID> noFall = new ArrayList<>();
	//pour les soeurs:
	@Getter @Setter private Player soeur1;
	@Getter @Setter private Player soeur2;
	@Getter @Setter private int msgCounter1;
	@Getter @Setter private int msgCounter2;
	@Getter private List<UUID> waitingResurection = new ArrayList<>();


	private Main main;

	public LgUtils(Main main) {
		this.main = main;
	}

	public boolean isInGame(Player p) {
		return main.joueurs.containsKey(p.getUniqueId());
	}

	public Joueur getJoueur(Player p) {
		return main.joueurs.get(p.getUniqueId());
	}

	public boolean isDead(Player p) {
		return main.death.contains(p.getUniqueId());
	}

	public void resurect(Player p) {
		if(isInGame(p)) {
			if(isDead(p))
				main.death.remove(p.getUniqueId());
			if(main.deathByLG.contains(p.getUniqueId()))
				main.deathByLG.remove(p.getUniqueId());
			//TODO Téleporter le joueur aléatoirement
		}
	}
	
	public void setWaitingResurection(Player p) {
		waitingResurection.add(p.getUniqueId());
		p.teleport(new Location(Bukkit.getWorld("world"), main.getConfig().getInt("resurect.x"),  main.getConfig().getInt("resurect.y"),  main.getConfig().getInt("resurect.z")));
		Bukkit.getScheduler().runTaskLater(main, () -> {
			waitingResurection.remove(p.getUniqueId());
			p.setGameMode(GameMode.SPECTATOR);
			main.death.add(p.getUniqueId());
		}, 200L);
	}

	public void addVote(Player voteur, Player cible) {
		removeVote(voteur);
		if(main.votes.containsKey(cible.getUniqueId())) {
			main.votes.get(cible.getUniqueId()).add(voteur.getUniqueId());
			if(getJoueur(voteur).getRole() == Role.CORBEAU) {
				main.votes.get(cible.getUniqueId()).add(voteur.getUniqueId());
				main.votes.get(cible.getUniqueId()).add(voteur.getUniqueId());
			}
		}else {
			main.votes.put(cible.getUniqueId(), new ArrayList<UUID>());
			main.votes.get(cible.getUniqueId()).add(voteur.getUniqueId());
			if(getJoueur(voteur).getRole() == Role.CORBEAU) {
				main.votes.get(cible.getUniqueId()).add(voteur.getUniqueId());
				main.votes.get(cible.getUniqueId()).add(voteur.getUniqueId());
			}
		}
		voteur.sendMessage("§aVotre vote a bien été enregistré.");
	}

	public void removeVote(Player p) {
		main.votes.forEach((uuid, list) -> {
			if(list.contains(p.getUniqueId())) {
				while(list.contains(p.getUniqueId())) {
					list.remove(p.getUniqueId());
				}
				p.sendMessage("§aVotre votre contre " + Bukkit.getPlayer(uuid) + " a été retiré.");
			}

		});
	}
	
	public Player getTopVoted() {
	    Comparator<? super UUID> maxValueComparator = (
	            entry1, entry2) -> new Integer(main.votes.get(entry1).size()).compareTo(
	            		new Integer(main.votes.get(entry2).size()));

		UUID uuid = main.votes.keySet().stream().max(maxValueComparator).get();
		return Bukkit.getPlayer(uuid);
	}
	
	
	public void setWinners() {
		for(UUID uuid : main.joueurs.keySet()) {
			if(!main.death.contains(uuid)) {
				main.winners.add(uuid);
			}
		}
		Joueur joueur = getJoueur(Bukkit.getPlayer(getWinners().get(0)));
		CampType camp = getJoueur(Bukkit.getPlayer(getWinners().get(0))).getCamp();
		if(camp == CampType.SOLO) {
			Bukkit.broadcastMessage("§a§lLa partie est terminée !");
			Bukkit.broadcastMessage("§a§lElle a été remportée par §2§l" + joueur.getPlayer().getName() + " §aqui était §2§l" + joueur.getRole().getName()  +" §a!");
		}else {
			Bukkit.broadcastMessage("§a§lLa partie est terminée !");
			Bukkit.broadcastMessage("§a§lElle a été remportée par le camps des §2§l" + joueur.getCamp().getName() + " §a!");
			Bukkit.broadcastMessage("§a§lFélicitations aux survivants :");
			getWinners().forEach((uuid) -> {
				Bukkit.broadcastMessage("§a- §2" + Bukkit.getPlayer(uuid).getName());
			});
		}
	}
	
	public List<UUID> getWinners(){
		return main.winners;
	}
	

}
