package fr.iban.lg.listeners;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import fr.iban.lg.Main;
import fr.iban.lg.objects.Camp.CampType;
import fr.iban.lg.objects.Kit.When;
import fr.iban.lg.objects.Joueur;
import fr.iban.lg.objects.Role;

public class PlayerDeathListener implements Listener {

	private Main main;

	public PlayerDeathListener(Main main) {
		this.main = main;
	}

	@EventHandler
	public void onDeath(PlayerDeathEvent e) {
		Player death = e.getEntity();
		if(main.utils.isInGame(death)) {
			Joueur joueurDeath = main.utils.getJoueur(death);
			if(e.getEntity().getKiller() != null) {
				Player killer = e.getEntity().getKiller();
				if(main.utils.isInGame(killer)) {
					Joueur joueurKiller = main.utils.getJoueur(killer);
					if(joueurKiller.getCamp() == CampType.LOUP) {
						if(joueurDeath.getCamp() != CampType.LOUP)
							main.deathByLG.add(death.getUniqueId());
						if(joueurKiller.getRole() != Role.MERCENAIRE) {
							killer.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 120, 0));
							killer.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 60, 0));
						}
					}else {
						if(joueurKiller.getRole() == Role.VOLEUR) {
							joueurKiller.setCamp(joueurDeath.getCamp());
							joueurKiller.setRole(joueurDeath.getRole());
							joueurKiller.setKit(joueurDeath.getKit());
							joueurKiller.setUsedPower(joueurDeath.isUsedPower());
							killer.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
							for(PotionEffect effect : joueurKiller.getKit().getEffects().keySet()) {
								if(joueurKiller.getKit().getEffects().get(effect) == When.ALLWAYS) {
									killer.addPotionEffect(effect);
								}
							}
						}
					}
				}
			}
			
			main.utils.setWaitingResurection(death);
			main.joueurs.forEach((a, b) -> {
				if(b.getRole() == Role.SORCIERE) {
					if(joueurDeath != b && !b.isUsedPower()) {
						Bukkit.getPlayer(a).sendMessage("§a" + death + " est mort, vous pouvez le ressussiter, vous avez 10 secondes.");
					}
				}
				if(b.getRole() == Role.INFECT_PERE_DES_LOUPS) {
					if(joueurDeath != b && !b.isUsedPower()) {
						Bukkit.getPlayer(a).sendMessage("§a" + death + " est mort, vous pouvez l'infecter, vous avez 10 secondes.");
					}
				}
			});
			if(joueurDeath.getRole() == Role.ANCIEN && !joueurDeath.isUsedPower()) {
				main.utils.resurect(death);
				joueurDeath.setUsedPower(true);
			}else {
				if(joueurDeath.getCamp() == CampType.VILLAGE) {
					Bukkit.broadcastMessage("§c§l=======================§4§l§c§l=======================");
					Bukkit.broadcastMessage("§2§lLe village a perdu un de ses membres: " + death.getName() +", qui était " + joueurDeath.getRole().getName() +".");
					Bukkit.broadcastMessage("§c§l===============================================");
				}
				
			}
			if(main.utils.getModele() != null) {
				if(death == main.utils.getModele()) {
					main.joueurs.forEach((uuid, joueur) -> {
						if(joueur.getRole() == Role.ENFANT_SAUVAGE) {
							joueur.setCamp(CampType.LOUP);
							joueur.getPlayer().sendMessage("§aVotre modèle est mort, vous rejoignez le camps des Loups-Garous !");
						}
					});
				}
			}
			
			
			//Check si la partie est terminée.
			int wolf = 0;
			int villager = 0;
			int solo = 0;
			int inlove = 0;
			for(UUID uuid : main.joueurs.keySet()) {
				Joueur joueur = main.joueurs.get(uuid);
				switch (joueur.getCamp()) {
				case LOUP:
					wolf++;
					break;
				case VILLAGE:
					villager++;
					break;
				case SOLO:
					solo++;
				case LOVE:
					inlove++;
				}
			}
			if(wolf == 0 && solo == 0 && inlove == 0) {
				//Le village a gagné
				main.utils.setWinners();
			}
			if(villager == 0 && solo == 0 && inlove == 0) {
				//Les loups ont gagné
				main.utils.setWinners();

			}
			if(wolf == 0 && villager == 0 && inlove == 0) {
				if(solo == 1) {
					//Le mec solo a gagné
					main.utils.setWinners();

				}
			}
			if(wolf == 0 && solo == 0 && villager == 0) {
				//Les amoureux ont gagné
				main.utils.setWinners();

			}
			
		}
	}
}
