package fr.iban.lg.listeners;

import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import fr.iban.lg.Main;

public class AsyncPlayerChatListener implements Listener {

	private Main main;

	public AsyncPlayerChatListener(Main main) {
		this.main = main;	
	}

	public void onChat(AsyncPlayerChatEvent e) {
		if(main.gameManager.isLgGame == true) {
			e.setCancelled(true);
		}
	}

}
