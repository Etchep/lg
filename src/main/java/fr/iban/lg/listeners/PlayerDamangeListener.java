package fr.iban.lg.listeners;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import fr.iban.lg.Main;

public class PlayerDamangeListener implements Listener {

	private Main main;

	public PlayerDamangeListener(Main main) {
		this.main = main;
	}
	
	@EventHandler
	public void onDamage(EntityDamageEvent e) {
		if(e.getEntityType() == EntityType.PLAYER) {
			Player p = (Player) e.getEntity();
			if(main.utils.getNoFall().contains(p.getUniqueId())) {
				if(e.getCause() == DamageCause.FALL) {
					e.setCancelled(true);
				}
			}
		}
	}

}
