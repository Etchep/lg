package fr.iban.lg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import fr.iban.lg.commands.ChasseurCMD;
import fr.iban.lg.commands.CupidonCMD;
import fr.iban.lg.commands.EnfantCMD;
import fr.iban.lg.commands.InfectCMD;
import fr.iban.lg.commands.LgCMD;
import fr.iban.lg.commands.RenardCMD;
import fr.iban.lg.commands.SalvateurCMD;
import fr.iban.lg.commands.SoeurCMD;
import fr.iban.lg.commands.SorciereCMD;
import fr.iban.lg.commands.VoteCMD;
import fr.iban.lg.commands.VoyanteCMD;
import fr.iban.lg.game.GameManager;
import fr.iban.lg.gui.CompoGUI;
import fr.iban.lg.listeners.AsyncPlayerChatListener;
import fr.iban.lg.listeners.PlayerDamangeListener;
import fr.iban.lg.listeners.PlayerDeathListener;
import fr.iban.lg.objects.Camp;
import fr.iban.lg.objects.Joueur;
import fr.iban.lg.objects.Role;
import fr.iban.lg.utils.ActionBar;
import fr.iban.lg.utils.ItemBuilder;
import fr.iban.lg.utils.LgUtils;

public class Main extends JavaPlugin {
	
	public LgUtils utils;
	public GameManager gameManager;
	public ActionBar actionBar;
	public ItemBuilder itemBuilder;
	public Camp camp;
	public Map<UUID, Joueur> joueurs;
	public List<Role> compo;
	public List<UUID> deathByLG;
	public List<UUID> death;
	public Map<UUID, List<UUID>> votes;
	public List<UUID> inLove;
	public List<UUID> winners;
	
	public void onEnable() {
		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(new AsyncPlayerChatListener(this), this);
		pm.registerEvents(new CompoGUI(this), this);
		pm.registerEvents(new PlayerDeathListener(this), this);
		pm.registerEvents(new PlayerDamangeListener(this), this);
		joueurs = new ConcurrentHashMap<UUID, Joueur>();
		votes = new HashMap<UUID, List<UUID>>();
		compo = new ArrayList<Role>();
		deathByLG = new ArrayList<>();
		death = new ArrayList<>();
		inLove = new ArrayList<>();
		winners = new ArrayList<>();
		gameManager = new GameManager(this);
		actionBar = new ActionBar();
		itemBuilder = new ItemBuilder();
		camp = new Camp();
		utils = new LgUtils(this);
		getCommand("lg").setExecutor(new LgCMD(this));
		getCommand("infecte").setExecutor(new InfectCMD(this));
		getCommand("voyante").setExecutor(new VoyanteCMD(this));
		getCommand("sorciere").setExecutor(new SorciereCMD(this));
		getCommand("chasseur").setExecutor(new ChasseurCMD(this));
		getCommand("cupidon").setExecutor(new CupidonCMD(this));
		getCommand("vote").setExecutor(new VoteCMD(this));
		getCommand("enfant").setExecutor(new EnfantCMD(this));
		getCommand("renard").setExecutor(new RenardCMD(this));
		getCommand("salvateur").setExecutor(new SalvateurCMD(this));
		getCommand("soeur").setExecutor(new SoeurCMD(this));
		this.saveDefaultConfig();
	}

}
