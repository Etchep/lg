package fr.iban.lg.game;

import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import fr.iban.lg.Main;
import fr.iban.lg.gui.CompoGUI;
import fr.iban.lg.objects.Joueur;
import fr.iban.lg.objects.Role;

public class GameManager {
	
	private Main main;
	
	public GameManager(Main main) {
		this.main = main;
	}
	
	public boolean isLgGame = false;
	
	public boolean votingTime = false;
	
    /**
     * Lancer le mode LG
     *
     * @param pvpstart
     *        Temps avant le début du pvp (secondes)
     */
	
	public void startLg(int pvpstart) {
		isLgGame = true;
		Bukkit.getWorld("world").setTime(0);
		new GameRunnable(main, pvpstart, this).runTaskTimer(main, 0L, 20L);
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "gamerule reducebuginfo true");
	}
	
	public void giveRoles() {
		List<Role> toGive = main.compo;
		Bukkit.getOnlinePlayers().forEach((player) -> {
			int random = new Random().nextInt(toGive.size());
			Role role = toGive.get(random);
			main.joueurs.put(player.getUniqueId(), new Joueur(main, player, role));
			if(role == Role.SOEUR) {
				if(main.utils.getSoeur1() == null) {
					main.utils.setSoeur1(player);
				}else {
					main.utils.setSoeur2(player);
				}
			}
			toGive.remove(random);
		});
	}
	
	//Ne pas oublier de vérifier que le joueur est l'opérateur de la partie.
	public void openCompoGUI(Player p) {
		p.openInventory(CompoGUI.inv);
	}
	
	public void startVoting() {
		this.votingTime = true;
		Bukkit.broadcastMessage("§aLe bureau de vote est §2§lOUVERT §a! Utilisez la commande §2§l/vote <joueur> §apour voter contre un joueur.");
		Bukkit.getScheduler().runTaskLater(main, () -> {
			this.votingTime = false;
			Bukkit.broadcastMessage("§aLe bureau de vote est désormais §4§lFERMÉ §a!\n" +
			"§aLe joueur §2" + main.utils.getTopVoted() + " §aa reçu le plus de votes ! (" + main.votes.get(main.utils.getTopVoted().getUniqueId()).size() +" votes). Il perd donc la moitiée de sa vie.");
		main.joueurs.forEach((player, joueur) -> {
			if(joueur.getRole() == Role.ANGE) {
				Bukkit.getPlayer(player).setHealth(Bukkit.getPlayer(player).getHealth() + 2D);
				Bukkit.getPlayer(player).sendMessage("§cVous avez reçu un coeur supplémentaire !");
			}
		});
		}, 1200);
	}

}
