package fr.iban.lg.game;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import fr.iban.lg.Main;
import fr.iban.lg.objects.Camp.CampType;
import fr.iban.lg.objects.Kit.When;
import fr.iban.lg.objects.Role;

public class GameRunnable extends BukkitRunnable {
	
	private Main main;
	private int timer = 0;
	private int episode = 1;
	private int pvpstart;
	private Location spawn = new Location(Bukkit.getWorld("world"), 0, 100, 0);
	private long currenttime;
	private GameManager manager;


	public GameRunnable(Main main, int pvpstart, GameManager manager) {
		this.main = main;
		this.pvpstart = pvpstart;
		this.manager = manager;
	}


	@Override
	public void run() {
		currenttime = Bukkit.getWorld("world").getTime();
		if(timer == 1200) {
			manager.giveRoles();
		}
		if(timer > 1200) {
			Bukkit.getWorld("world").setTime(currenttime + 20);
			if(currenttime >= 0 && currenttime <= 20) {
				//Le joueur se lève
				
				main.joueurs.forEach((uuid, joueur) -> {
					Player p = Bukkit.getPlayer(uuid);
					for(PotionEffect effect : joueur.getKit().getEffects().keySet()) {
						if(joueur.getKit().getEffects().get(effect) == When.NIGHT) {
							if(p.hasPotionEffect(effect.getType()))
								p.removePotionEffect(effect.getType());
						}
						if(joueur.getKit().getEffects().get(effect) == When.DAY) {
							p.addPotionEffect(effect);
						}
					}
					if(joueur.getRole() == Role.VOYANTE) joueur.setUsedPower(false);
					if(joueur.getRole() == Role.SALVATEUR) joueur.setUsedPower(false);
					if(joueur.getRole() == Role.MONTREUR_D_OURS) {
						for(Entity entity : p.getNearbyEntities(50D, 50D, 50D)) {
							if(entity.getType() == EntityType.PLAYER) {
								Player player = (Player) entity;
								if(main.utils.getJoueur(player).getCamp() == CampType.LOUP)
								Bukkit.broadcastMessage("§b§l[§6§lLoupsGarous§b§l] §6Grrrrrrrrrrrr !§6");
							}
						}
					}
					main.utils.setMsgCounter1(2);
					main.utils.setMsgCounter2(2);
				});
			}
			if(currenttime >= 12000 && currenttime <= 12020) {
				//Début de la nuit
				main.joueurs.forEach((uuid, joueur) -> {
					Player p = Bukkit.getPlayer(uuid);
					for(PotionEffect effect : joueur.getKit().getEffects().keySet()) {
						if(joueur.getKit().getEffects().get(effect) == When.DAY) {
							if(p.hasPotionEffect(effect.getType()))
								p.removePotionEffect(effect.getType());
						}
						if(joueur.getKit().getEffects().get(effect) == When.NIGHT) {
							p.addPotionEffect(effect);
						}
					}
					if(joueur.getRole() == Role.PETITE_FILLE) {
						for(Entity entity : p.getNearbyEntities(100D, 100D, 100D)) {
							if(entity.getType() == EntityType.PLAYER) {
								Player player = (Player) entity;
								p.sendMessage("§2" + player.getName() + " §a se trouve dans les parrages !");
							}
						}
					}
				});
			}
			if(timer == 1200 + 60*5) {
				main.joueurs.forEach((a, b) ->{
					if(b.getRole() == Role.ENFANT_SAUVAGE) {
						b.setUsedPower(true);
					}
				});
			}
			
			main.joueurs.forEach((p, j) -> {
				Player player = Bukkit.getPlayer(p);
				if(j.getRole() == Role.SOEUR) {
					for(Entity entity : player.getNearbyEntities(10D, 10D, 10D)) {
						if(entity.getType() == EntityType.PLAYER) {
							Player player2 = (Player) entity;
							if(main.utils.getJoueur(player2).getRole() == Role.SOEUR) {
								player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 5, 0, false, false));
							}
						}
					}
				}
			});
		}
		if(timer > pvpstart) {
			//Afficher et mettre à jour l'action bar
			Bukkit.getOnlinePlayers().forEach((player) -> {
				int distance = (int) spawn.distance(player.getLocation());
				if(distance <= 300) {
					main.actionBar.sendActionBar(player, "§d§lDistance §dau centre §6§l➤ Entre 0 et 300 blocs");
				}else if(distance > 300 && distance <= 600) {
					main.actionBar.sendActionBar(player, "§d§lDistance §dau centre §6§l➤ Entre 300 et 600 blocs");
				}else if(distance > 600 && distance <= 1200) {
					main.actionBar.sendActionBar(player, "§d§lDistance §dau centre §6§l➤ Entre 600 et 1200 blocs");
				}else if(distance > 1200) {
					main.actionBar.sendActionBar(player, "§d§lDistance §dau centre §6§l➤  Plus de 1200 blocs");
				}
			});
		}
		episode = timer/1200;
		if(episode < 1) episode = 1;
		timer++;
	}

}
