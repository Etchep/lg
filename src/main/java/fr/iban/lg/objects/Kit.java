package fr.iban.lg.objects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import fr.iban.lg.Main;
import lombok.Getter;

public class Kit {

	public enum When {
		ALLWAYS,
		NIGHT,
		DAY
	}
	
	@Getter private List<ItemStack> items = new ArrayList<>(); 
	@Getter private Map<PotionEffect, When> effects = new HashMap<>();

	public Kit(Main main, Role role) {
		switch (role) {
		case SIMPLE_VILLAGOIS:
			break;
		case CHASSEUR:
			this.items.add(main.itemBuilder.buildItem(Material.BOW, "", 1, 0, null, Arrays.asList(Enchantment.ARROW_DAMAGE), Arrays.asList(4)));
			this.items.add(main.itemBuilder.buildItem(Material.ARROW, "", 64, 0, null, null, null));
			this.items.add(main.itemBuilder.buildItem(Material.ARROW, "", 64, 0, null, null, null));
			this.items.add(main.itemBuilder.buildItem(Material.MONSTER_EGG, "", 3, 95, null, null, null));
			this.items.add(main.itemBuilder.buildItem(Material.BONE, "", 15, 0, null, null, null));
			break;
		case VOYANTE:
			this.items.add(main.itemBuilder.buildItem(Material.BOOKSHELF, "", 4, 0, null, null, null));
			this.items.add(main.itemBuilder.buildItem(Material.OBSIDIAN, "", 4, 0, null, null, null));
			this.getEffects().put(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 0, false, false), When.ALLWAYS);
			break;
		case LOUP_BLANC:
			this.getEffects().put(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 0, false, false), When.ALLWAYS);
			this.getEffects().put(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 0, false, false), When.NIGHT);

			break;
		case INFECT_PERE_DES_LOUPS:
			this.getEffects().put(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 0, false, false), When.ALLWAYS);
			this.getEffects().put(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 0, false, false), When.NIGHT);
			break;
		case LOUP_GAROU:
			this.getEffects().put(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 0, false, false), When.ALLWAYS);
			this.getEffects().put(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 0, false, false), When.NIGHT);
			break;
		case SORCIERE:
			this.items.add(main.itemBuilder.buildItem(Material.POTION, "", 3, 16389, null, null, null));
			this.items.add(main.itemBuilder.buildItem(Material.POTION, "", 1, 16385, null, null, null));
			this.items.add(main.itemBuilder.buildItem(Material.POTION, "", 3, 16396, null, null, null));
			break;
		case CUPIDON:
			this.items.add(main.itemBuilder.buildItem(Material.BOW, "", 1, 0, null, Arrays.asList(Enchantment.ARROW_KNOCKBACK), Arrays.asList(1)));
			this.items.add(main.itemBuilder.buildItem(Material.ARROW, "", 64, 0, null, null, null));
			break;
		case PETITE_FILLE:
			this.items.add(main.itemBuilder.buildItem(Material.TNT, "", 5, 0, null, null, null));
			this.getEffects().put(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 0, false, false), When.ALLWAYS);
			this.getEffects().put(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 0, false, false), When.NIGHT);
			this.getEffects().put(new PotionEffect(PotionEffectType.WEAKNESS, Integer.MAX_VALUE, 0, false, false), When.NIGHT);
			break;
		case ASSASSIN:
			ItemStack power3 = new ItemStack(Material.ENCHANTED_BOOK);
			EnchantmentStorageMeta meta = (EnchantmentStorageMeta) power3.getItemMeta();
			meta.addStoredEnchant(Enchantment.ARROW_DAMAGE, 3, true);
			power3.setItemMeta(meta);
			ItemStack sharpness3 = new ItemStack(Material.ENCHANTED_BOOK);
			EnchantmentStorageMeta meta2 = (EnchantmentStorageMeta) sharpness3.getItemMeta();
			meta2.addStoredEnchant(Enchantment.DAMAGE_ALL, 3, true);
			sharpness3.setItemMeta(meta2);
			ItemStack protection3 = new ItemStack(Material.ENCHANTED_BOOK);
			EnchantmentStorageMeta meta3 = (EnchantmentStorageMeta) protection3.getItemMeta();
			meta3.addStoredEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3, true);
			protection3.setItemMeta(meta3);
			this.items.add(power3);
			this.items.add(protection3);
			this.items.add(sharpness3);
			this.getEffects().put(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 0, false, false), When.DAY);

			break;
		case SALVATEUR:
			this.items.add(main.itemBuilder.buildItem(Material.POTION, "", 2, 16389, null, null, null));
			break;
		case SOEUR:
			break;
		case ANCIEN:
			this.getEffects().put(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.MAX_VALUE, 0, false, false), When.NIGHT);
			break;
		case ANGE:
			break;
		case CITOYEN:
			break;
		case CORBEAU:
			break;
		case ENFANT_SAUVAGE:
			break;
		case MERCENAIRE:
			break;
		case MINEUR:
			this.items.add(main.itemBuilder.buildItem(Material.DIAMOND_PICKAXE, "", 2, 0, null, Arrays.asList(Enchantment.DIG_SPEED), Arrays.asList(2)));
			this.getEffects().put(new PotionEffect(PotionEffectType.FAST_DIGGING, Integer.MAX_VALUE, 1, false, false), When.ALLWAYS);
			break;
		case MONTREUR_D_OURS:
			break;
		case RENARD:
			this.getEffects().put(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 0, false, false), When.ALLWAYS);
			break;
		case VILAIN_PETIT_LOUP:
			this.getEffects().put(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 0, false, false), When.ALLWAYS);
			this.getEffects().put(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 0, false, false), When.NIGHT);
			this.getEffects().put(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 0, false, false), When.NIGHT);
			break;
		case VOLEUR:
			this.getEffects().put(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.MAX_VALUE, 0, false, false), When.NIGHT);
			break;
		default:
			break;
		}
	}
}
