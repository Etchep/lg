package fr.iban.lg.objects;

public class Camp {

	public enum CampType {
		LOUP("§9§lVotre objectif est de tuer les villageois.", "Loups-Garous"),
		VILLAGE("§9§lVotre objectif est d'éliminer les Loups-Garous.", "Villagois"),
		SOLO("§9§lVotre objectif est de gagner seul, vous ne faites partie d'aucun camp.", "Solo"),
		LOVE("§d§lKeur keur", "Amoureux");
		
		private String message = "";
		private String name = "";

		CampType(String message, String name) {
			this.message = message;
			this.name = name;
		}

		public String getMessage() {
			return message;
		}
		
		public String getName() {
			return name;
		}
	}
	

	public CampType getCamp(Role role) {
		switch (role) {
		case SIMPLE_VILLAGOIS:
			return CampType.VILLAGE;
		case CHASSEUR:
			return CampType.VILLAGE;
		case LOUP_GAROU:
			return CampType.LOUP;
		case LOUP_BLANC:
			return CampType.SOLO;
		case INFECT_PERE_DES_LOUPS:
			return CampType.LOUP;
		case VOYANTE:
			return CampType.VILLAGE;
		case SORCIERE:
			return CampType.VILLAGE;
		case CUPIDON:
			return CampType.VILLAGE;
		case PETITE_FILLE:
			return CampType.VILLAGE;
		case ASSASSIN:
			return CampType.SOLO;
		case SALVATEUR:
			return CampType.VILLAGE;
		case ANCIEN:
			return CampType.VILLAGE;
		case ANGE:
			return CampType.VILLAGE;
		case CITOYEN:
			return CampType.VILLAGE;
		case CORBEAU:
			return CampType.VILLAGE;
		case ENFANT_SAUVAGE:
			return CampType.VILLAGE;
		case MERCENAIRE:
			return CampType.LOUP;
		case MINEUR:
			return CampType.VILLAGE;
		case MONTREUR_D_OURS:
			return CampType.VILLAGE;
		case RENARD:
			return CampType.VILLAGE;
		case SOEUR:
			return CampType.VILLAGE;
		case VILAIN_PETIT_LOUP:
			return CampType.LOUP;
		case VOLEUR:
			return CampType.VILLAGE;
		default:
			return CampType.SOLO;
		}
	}

}
