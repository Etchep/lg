package fr.iban.lg.objects;

import java.util.List;
import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

import fr.iban.lg.Main;
import fr.iban.lg.objects.Camp.CampType;
import fr.iban.lg.objects.Kit.When;
import lombok.Getter;
import lombok.Setter;

public class Joueur {
	
	@Getter @Setter private Player player;
	@Getter @Setter private Role role;
	@Getter @Setter private Kit kit;
	@Getter @Setter private CampType camp;
	@Getter @Setter private boolean usedPower = false;
	@Getter @Setter private boolean infected = false;
	
	public Joueur(Main main, Player p, Role role) {
		this.role = role;
		this.kit = new Kit(main, role);
		this.camp = main.camp.getCamp(role);
		this.player = p;
		setupRole();
	}
	
	public boolean isInventoryFull(Player p){
		return p.getInventory().firstEmpty() == -1;
	}
	
	public void setupRole() {
		List<ItemStack> items = this.getKit().getItems();
		Map<PotionEffect, When> effects = this.getKit().getEffects();
		if(items != null && !items.isEmpty()) {
			for(ItemStack it : items) {
				if(!isInventoryFull(this.player)) {
					player.getInventory().addItem(it);
				}else {
					player.getWorld().dropItem(player.getLocation(), it);
				}
			}
		}
		if(effects != null && !effects.isEmpty()) {
			for(PotionEffect effect : effects.keySet()) {
				if(effects.get(effect) == When.ALLWAYS) 
					player.addPotionEffect(effect);
			}
		}
		if(this.getRole() == Role.LOUP_BLANC) player.setMaxHealth(player.getMaxHealth() + (player.getMaxHealth()/2));
		player.sendMessage("§b§l[§6§lLoupsGarous§b§l]"+ " §eVous êtes §6§l" + this.getRole().getName() + " §e!"  
		+ " \n" + this.getCamp().getMessage() 
		+ " \n" + this.getRole().getMessage());
	}

}
