package fr.iban.lg.objects;

import lombok.Getter;

public enum Role {
	
	SIMPLE_VILLAGOIS("Simple Villagois", "Vous ne disposez d'aucun pouvoir particulier."),
	LOUP_GAROU("Loup-Garou", "Pour ce faire, vous disposez des effets Strength I (la nuit) et Night Vision. A chaque kill, vous gagnez pendant 1 minute de Speed et 2 coeurs d'absorption pendant 2 minutes."),
	CHASSEUR("Chasseur", "Pour ce faire, vous disposez d'un arc Power IV, de 128 fleches, de 3 oeufs de loups et de 15 os. A votre mort, vous pouvez tirer sur quelqu'un pour lui faire perdre la moitie de sa vie, a l'aide de la commande /chasseur <joueur>."),
	LOUP_BLANC("Loup-Garous Blanc", "Pour ce faire, vous disposez des effets Strength I (la nuit) et Night vision, ainsi que de 5 coeurs supplémentaires. A chaque kill, vous gagnez 1 minute de Speed et 2 coeurs d'absorption pendant 2 minutes."),
	INFECT_PERE_DES_LOUPS("Infect-Pere-Des-Loups", "Pour ce faire, vous disposez des effets Strength I et Night Vision durant la nuit. A chaque Kill, vous gagnez 1 minute de Speed et 2 coeurs d'absorption pendant 2 minutes."),
	VOYANTE("Voyante", "Pour ce faire, vous disposez de l'effet Night Vision, de 4 bibliothèques et de 4 blocs d'obsidienne. A chaque début de journée, vous pourrez connaitre le rôle d'un joueur a l'aide de la commande /voyante <joueur>."),
	SORCIERE("Sorcière", "Pour ce faire, vous disposez de 3 potions splash d'Instant Health I, d'une potion splash de Régéneration I et de 3 potions splash d'Instant Damage I. Vous avez le pouvoir de ressusiter un joueur mort une fois dans la partie, a l'aide de la commande /sorciere <joueur>."),
	CUPIDON("Cupidon", "Pour ce faire, vous disposez d'un arc Punch I et de 64 flèches. En début de partie, vous choisissez les 2 joueurs en couple avec la commande /cupidon <joueur> <joueur>."),
	PETITE_FILLE("Petite fille", "Vous disposez de l'effet Night Vision en permanence, ainsi que des effets Invisibility et Weakness I durant la nuit. Vous disposez également de 5 TNT. Au crépuscule, vous connaîtrez les pseudos des joueurs se situant dans un rayon de 100 blocs autour de vous."),
	ASSASSIN("Assassin", "Pour ce faire, vous disposez de trois livres d'enchantement  (Power III, Sharpness III, Protection III) et de l'effet strength I (le jour)."),
	SALVATEUR("Salvateur", "Pour ce faire, vous disposez de 2 potions splash d'Instant health. A chaque début de journée, vous pouvez choisir un joueur (y compris vous) à qui vous conférez NoFall et Résistance pendant 20 minutes, à l'aide de la commande /salvateur <joueur>. Vous ne pouvez pas choisir 2 fois d'affilée le même joueur."),
	SOEUR("Soeur", "Pour ce faire, vous disposez d'un allié qui a le même rôle que vous. Deux fois pas épisode, vous pourrez vous échanger deux messages chacun. vous obtenez résistance I lorsque vous serrez à 10 blocs ou moins de votre sœur."),
	RENARD("Renard", "Pour ce faire, vous disposez de l'effet Speed I. Jusqu'à 3 fois dans la partie, vous pouvez flairer un joueur qui se trouve à moins de 10 blocs de vous, avec la commande /renard <joueur>. Vous saurez ainsi si le joueur est un loup ou un innocent."),
	MONTREUR_D_OURS("Montreur d'ours", "Pour ce faire, vous disposez d'un pouvoir puissant : à chaque début d'épisode, si un Loup se trouve dans un rayon de 50 blocs autour de vous, vous grognez et tous les joueurs le voient."),
	ENFANT_SAUVAGE("Enfant sauvage", "Vous choisissez un modèle parmi les joueurs (commande : /enfant <pseudo>). Si celui-ci meurt, vous devenez un Loup-Garou et devez gagner avec eux"),
	ANGE("Ange", "Pour ce faire, vous disposez d'un avantage lors du vote. En effet, si vous avez plus de deux votes orientés contre vous, vous gagnerez 1 coeur supplémentaire. Mais attention, vous perdrez de la vie et serez vulnérable si vous accumulez trop de votes contre vous."),
	VILAIN_PETIT_LOUP("Vilain Petit Loup", "Pour ce faire, vous disposez des effets Strength I (la nuit) et Night Vision. A chaque kill, vous gagnerez 2 coeurs d'absorption pendant 2 minutes. Vous disposez également de l'effet Speed I la nuit, attention à ne pas vous faire repérer !"),
	ANCIEN("Ancien", "Pour ce faire, vous disposez de l'effet Résistance. Si vous mourrez de la main d'un Loup-Garou, vous survivrez la première fois et serez téléporté ailleurs."),
	VOLEUR("Voleur", "Cependant, vous récuperez le rôle de la première personne que vous tuerez ainsi que les pouvoirs qu'elle avait encore. Si cette personne était en couple, vous lui volez également son âme-soeur qui ne mourra donc pas. Vous devrez alors gagner avec votre nouveau camp. La voyante vous verra en tant que Voleur et le Renard vous pensera gentil quel que soit votre nouveau rôle. Pour vous aider, vous avez l'effet Résistance I jusqu'à ce que vous voliez un rôle."),
	MINEUR("Mineur", "Pour ce faire, vous disposez de l'effet Haste II ainsi que d'une pioche en diamant avec l'enchantement efficacité II."),
	CITOYEN("Citoyen", "Pour ce faire, vous pouvez, avant l'annonce du résultat du vote, visualiser les votes de tous les joueurs, et ce une seule fois dans la partie."),
	CORBEAU("Corbeau", "Pour ce faire, vous pouvez jeter a chaque vote une malediction sur un joueur : votre vote comptera alors triple."),
	MERCENAIRE("Mercenaire", "Vous etes un Simple-Villageois allie aux Loup-Garous, vous ne connaissez pas leur identite et ils ne connaissent pas la votre. Vous ne beneficiez pas des effets des Loups-Garous. A vous de vous servir de votre pouvoir de persuasion afin de convaincre les Loups-Garous que vous n'est pas un de leurs ennnemis.")	;
	
	@Getter private String name = "";
	@Getter private String message = "";
	
	private Role(String name, String message) {
		this.name = name;
		this.message = message;
	}
	
	public static Role find(String name) {
	    for (Role role : Role.values()) {
	    	if(role.name().contains(name)) {
	    		return role;
	    	}
	    }
	    return null;
	}
	

}
