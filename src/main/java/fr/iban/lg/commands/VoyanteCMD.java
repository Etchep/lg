package fr.iban.lg.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.iban.lg.Main;
import fr.iban.lg.objects.Joueur;
import fr.iban.lg.objects.Role;

public class VoyanteCMD implements CommandExecutor {

	private Main main;

	public VoyanteCMD(Main main) {
		this.main = main;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player)sender;
			if(main.gameManager.isLgGame == true) {
				if(main.utils.isInGame(p)) {
					Joueur joueur = main.utils.getJoueur(p);
					if(joueur.getRole() == Role.VOYANTE) {
						if(args.length == 0) {
							p.sendMessage("§c/voyante <Joueur>");
						}else if(args.length == 1) {
							if(!joueur.isUsedPower()) {
								Player target = Bukkit.getPlayer(args[0]);
								if(target != null) {
									if(main.utils.isInGame(target)) {
										Joueur joueurTarget = main.utils.getJoueur(target);
										if(!main.death.contains(target.getUniqueId())) {
											joueur.setUsedPower(true);
											p.sendMessage("§aLe rôle de " + target.getName() + " est §2§l" + joueurTarget.getRole().getName() + "§a.");
										}else { 
											p.sendMessage("§cCe joueur est mort.");
										}		
									}else {
										p.sendMessage("§cCette personne ne joue pas.");
									}
								}else {
									p.sendMessage("§cCe joueur n'est pas en ligne !");
								}
							}else {
								p.sendMessage("§cVous avez déjà utilisé votre pouvoir !");
							}
						}
					}else {
						p.sendMessage("§cVous n'êtes pas " + Role.VOYANTE.getName() + " !");
					}
				}
			}else {
				p.sendMessage("§cLe mode Loup-Garous n'est pas activé !");
			}
		}
		return false;
	}

}
