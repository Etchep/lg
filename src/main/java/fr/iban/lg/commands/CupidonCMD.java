package fr.iban.lg.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.iban.lg.Main;
import fr.iban.lg.objects.Role;
import fr.iban.lg.objects.Camp.CampType;

public class CupidonCMD implements CommandExecutor {

	private Main main;
	
	public CupidonCMD(Main main) {
		this.main = main;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player)sender;
			if(main.gameManager.isLgGame == true) {
				if(main.utils.isInGame(p)) {
					if(main.utils.getJoueur(p).getRole() == Role.CUPIDON) {
						if(!main.utils.isDead(p)) {
							if(args.length == 0) {
								p.sendMessage("§c/cupidon <joueur> <joueur>");
							}else if(args.length == 2) {
								Player target1 = Bukkit.getPlayer(args[0]);
								Player target2 = Bukkit.getPlayer(args[1]);
								if(target1 != null && main.utils.isInGame(target1) && !main.utils.isDead(target1)) {
									if(target2 != null && main.utils.isInGame(target2) && !main.utils.isDead(target2)) {
										if(main.inLove.isEmpty()) {
											p.sendMessage("§aVous venez de former un nouveau couple !");
											main.inLove.add(target1.getUniqueId());
											main.inLove.add(target2.getUniqueId());
											if(main.utils.getJoueur(target1).getCamp() != main.utils.getJoueur(target2).getCamp()) {
												main.utils.getJoueur(target1).setCamp(CampType.LOVE);
												main.utils.getJoueur(target2).setCamp(CampType.LOVE);
											}
										}else {
											p.sendMessage("§cLe couple a déjà été formé !");
										}
									}else {
										p.sendMessage("§cLe joueur " + args[1] + "n'a pas été trouvé !");
									}
								}else {
									p.sendMessage("§cLe joueur " + args[0] + "n'a pas été trouvé !");
								}
							}
						}else {
							p.sendMessage("§cVous êtes mort !");
						}
					}else {
						p.sendMessage("§cVous n'êtes pas cupidon !");
					}
				}
			}else {
				p.sendMessage("§cLe mode Loup-Garous n'est pas activé !");
			}
		}
		return false;
	}

}
