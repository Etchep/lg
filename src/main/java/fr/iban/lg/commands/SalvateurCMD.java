package fr.iban.lg.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import fr.iban.lg.Main;
import fr.iban.lg.objects.Joueur;
import fr.iban.lg.objects.Role;

public class SalvateurCMD implements CommandExecutor {

	private Main main;
	
	public SalvateurCMD(Main main) {
		this.main = main;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player)sender;
			if(main.gameManager.isLgGame == true) {
				if(main.utils.isInGame(p)) {
					Joueur joueur = main.utils.getJoueur(p);
					if(joueur.getRole() == Role.SALVATEUR) {
						if(!joueur.isUsedPower()) {
							if(args.length == 0) {
								p.sendMessage("§c/salvateur <joueur>");
							}else if(args.length == 1) {
								Player target = Bukkit.getPlayer(args[0]);
								if(target != null && main.utils.isInGame(target) && main.utils.isDead(p)) {
									target.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 60*20, 0, false, false));
									main.utils.getNoFall().add(target.getUniqueId());
									Bukkit.getScheduler().runTaskLater(main, () -> {
										main.utils.getNoFall().remove(target.getUniqueId());
									}, 60*20*20);
									joueur.setUsedPower(true);
								}
							}
						}else {
							p.sendMessage("§cVous avez déjà épuisé votre pouvoir !");
						}
					}
				}
			}else {
				p.sendMessage("§cLe mode Loup-Garous n'est pas activé !");
			}
		}
		return false;
	}

}
