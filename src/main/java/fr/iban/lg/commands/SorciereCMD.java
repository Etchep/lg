package fr.iban.lg.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.iban.lg.Main;
import fr.iban.lg.objects.Joueur;
import fr.iban.lg.objects.Role;

public class SorciereCMD implements CommandExecutor {
	
	private Main main;
	
	public SorciereCMD(Main main) {
		this.main = main;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player)sender;
			if(main.gameManager.isLgGame == true) {
				if(main.utils.isInGame(p)) {
					Joueur joueur = main.utils.getJoueur(p);
					if(joueur.getRole() == Role.SORCIERE) {
						if(args.length == 0) {
							p.sendMessage("§c/sorciere <joueur>");
						}else if(args.length == 1) {
							if(!joueur.isUsedPower()) {
								Player target = Bukkit.getPlayer(args[0]);
								if(target != null) {
									if(target != p) {
										if(main.utils.isInGame(target)) {
											if(main.utils.getWaitingResurection().contains(target.getUniqueId())) {
												main.utils.resurect(target);
												p.sendMessage("§aVous avez ressussité §2" + target.getName() + "§a.");
												target.sendMessage("§aVous avez été ressussité !");
											}else {
												p.sendMessage("§cCe joueur n'est pas mort !");
											}
										}else {
											p.sendMessage("§cCe joueur n'est pas dans la partie !");
										}
									}else {
										p.sendMessage("§cVous ne pouvez pas vous réanimer vous même !");
									}
								}else {
									p.sendMessage("§cCe joueur n'est pas en ligne !");
								}
							}else {
								p.sendMessage("§cVous avez déjà utilisé votre pouvoir !");
							}
						}
					}else {
						p.sendMessage("§cVous n'êtes pas sorcière !");
					}
				}
			}else {
				p.sendMessage("§cLe mode Loup-Garous n'est pas activé !");
			}
		}
		return false;
	}

}
