package fr.iban.lg.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import fr.iban.lg.Main;
import fr.iban.lg.objects.Joueur;
import fr.iban.lg.objects.Role;

public class SoeurCMD implements CommandExecutor {

	private Main main;

	public SoeurCMD(Main main) {
		this.main = main;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player)sender;
			if(main.gameManager.isLgGame == true) {
				if(main.utils.isInGame(p)) {
					Joueur joueur = main.utils.getJoueur(p);
					if(joueur.getRole() == Role.SOEUR) {
						if(args.length == 0) {
							p.sendMessage("§c/soeur <message>");
						}else if(args.length >= 1) {
				    		   StringBuilder msg = new StringBuilder();
							   for(String part : args){
								   msg.append(part + " ");
							   }
							if(main.utils.getSoeur1() == p) {
								if(main.utils.getMsgCounter1() >= 1) {
									main.utils.getSoeur2().sendMessage("§6" + p.getName() + " :§e " + msg);
									main.utils.setMsgCounter1(main.utils.getMsgCounter1() - 1);
								}else {
									p.sendMessage("§cVous avez épuisé votre pouvoir !");
								}
							}else if(main.utils.getSoeur2() == p) {
								if(main.utils.getMsgCounter2() >= 1) {
									main.utils.getSoeur1().sendMessage("§6" + p.getName() + " :§e " + msg);
									main.utils.setMsgCounter2(main.utils.getMsgCounter2() - 1);
								}else {
									p.sendMessage("§cVous avez épuisé votre pouvoir !");
								}
							}
						}
					}
				}
			}else {
				p.sendMessage("§cLe mode Loup-Garous n'est pas activé !");
			}
		}
		return false;
	}

}
