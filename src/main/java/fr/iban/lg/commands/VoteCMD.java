package fr.iban.lg.commands;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.iban.lg.Main;
import fr.iban.lg.objects.Role;

public class VoteCMD implements CommandExecutor {

	private Main main;
	
	public VoteCMD(Main main) {
		this.main = main;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player)sender;
			if(main.gameManager.isLgGame == true) {
				if(main.utils.isInGame(p) && !main.utils.isDead(p)) {
					if(main.gameManager.votingTime == true) {
						if(args.length == 0) {
							p.sendMessage("§c/vote <joueur>");
						}else if(args.length == 1) {
							if(args[0].equalsIgnoreCase("voir")) {
								if(main.utils.getJoueur(p).getRole() == Role.CITOYEN) {
									if(main.utils.getJoueur(p).isUsedPower() == false) {
										p.sendMessage("§aVoici les votes actuels :");
										for(UUID uuid : main.votes.keySet()) {
											p.sendMessage("§a- §2" + Bukkit.getPlayer(uuid).getName() + " §a: §2" + main.votes.get(uuid).size() + " votes");
										}
									}else {
										p.sendMessage("§cVous avez déjà utilisé votre pouvoir !");
									}
								}else {
									p.sendMessage("§cVous devez être Citoyen pour cela !");
								}
							}else {
								Player target = Bukkit.getPlayer(args[0]);
								if(target != null) {
									if(main.utils.isInGame(target)) {
										if(!main.utils.isDead(target)) {
											main.utils.addVote(p, target);
										}else {
											p.sendMessage("§cCe joueur est mort !");
										}
									}else {
										p.sendMessage("§cCe joueur n'est pas dans la partie.");
									}
								}else {
									p.sendMessage("§cCe joueur n'est pas en ligne !");
								}
							}
						}
					}else {
						p.sendMessage("§cL'heure n'est pas à la démocratie !");
					}
				}
			}else {
				p.sendMessage("§cLe mode Loup-Garous n'est pas activé !");
			}
		}
		return false;
	}

}
