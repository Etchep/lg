package fr.iban.lg.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.iban.lg.Main;
import fr.iban.lg.objects.Role;

public class EnfantCMD implements CommandExecutor {

	private Main main;
	
	public EnfantCMD(Main main) {
		this.main = main;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player)sender;
			if(main.gameManager.isLgGame == true) {
				if(main.utils.isInGame(p)) {
					if(main.utils.getJoueur(p).getRole() == Role.ENFANT_SAUVAGE) {
						if(!main.utils.getJoueur(p).isUsedPower()) {
							if(args.length == 0) {
								p.sendMessage("§c/enfant <joueur>");
							}else if(args.length == 1) {
								Player target = Bukkit.getPlayer(args[0]);
								if(target != null && main.utils.isInGame(p) && !main.utils.isDead(p) && target != p) {
									p.sendMessage("§aVous avez pris " + target.getName() + " comme modèle.");
									main.utils.setModele(target);
									main.utils.getJoueur(p).setUsedPower(true);
								}else {
									p.sendMessage("§cLe joueur n'a pas été trouvé.");
								}
							}
						}
					}
				}
			}else {
				p.sendMessage("§cLe mode Loup-Garous n'est pas activé !");
			}
		}
		return false;
	}

}
