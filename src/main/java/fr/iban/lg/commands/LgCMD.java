package fr.iban.lg.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.iban.lg.Main;
import fr.iban.lg.objects.Camp.CampType;
import fr.iban.lg.objects.Joueur;

public class LgCMD implements CommandExecutor{

	private Main main;

	public LgCMD(Main main) {
		this.main = main;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player)sender;
			if(main.gameManager.isLgGame == true) {
				if(main.utils.isInGame(p)) {
					Joueur joueur = main.utils.getJoueur(p);
					if(args.length == 1) {
						if(joueur.getCamp() == CampType.LOUP) {
							p.sendMessage("§aVoici la liste des loups :");
							main.joueurs.forEach((a, b) -> {
								if(b.getCamp() == CampType.LOUP) {
									p.sendMessage("§a- " + Bukkit.getPlayer(a).getName());
								}
							});
						}
					}
				}
			}else {
				p.sendMessage("§cLe mode Loup-Garous n'est pas activé !");
			}
		}
		return false;
	}

}
