package fr.iban.lg.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.iban.lg.Main;
import fr.iban.lg.objects.Role;

public class ChasseurCMD implements CommandExecutor {

	private Main main;

	public ChasseurCMD(Main main) {
		this.main = main;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player)sender;
			if(main.gameManager.isLgGame == true) {
				if(main.utils.isInGame(p)) {
					if(main.utils.getJoueur(p).getRole() == Role.CHASSEUR) {
						if(!main.utils.getJoueur(p).isUsedPower()) {
							if(main.utils.isDead(p)) {
								if(args.length == 0) {
									p.sendMessage("§c/chasseur <joueur>");
								}else if(args.length == 1) {
									Player target = Bukkit.getPlayer(args[0]);
									if(target != null) {
										if(main.utils.isInGame(target)) {
											if(!main.utils.isDead(target)) {
												Bukkit.broadcastMessage("§4§lPAM ! §cLe chasseur a tiré sur " + target.getName() + " avant de rendre son dernier souffle.");
												target.setHealth(target.getHealth()/2);
												main.utils.getJoueur(p).setUsedPower(true);
											}else {
												p.sendMessage("§cCe joueur est déjà mort !");
											}
										}else {
											p.sendMessage("§cCe joueur n'est pas dans la partie");
										}
									}else {
										p.sendMessage("§cCe joueur n'est pas en ligne.");
									}
								}
							}else {
								p.sendMessage("§cVous devez être mort pour utiliser cette commande.");
							}
						}
					}else {
						p.sendMessage("§cVous n'êtes pas chasseur !");
					}
				}
			}else {
				p.sendMessage("§cLe mode Loup-Garous n'est pas activé !");
			}
		}
		return false;
	}

}
