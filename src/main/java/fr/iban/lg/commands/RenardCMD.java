package fr.iban.lg.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.iban.lg.Main;
import fr.iban.lg.objects.Role;

public class RenardCMD implements CommandExecutor {

	private Main main;
	
	public RenardCMD(Main main) {
		this.main = main;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player)sender;
			if(main.gameManager.isLgGame == true) {
				if(main.utils.isInGame(p)) {
					if(main.utils.getJoueur(p).getRole() == Role.RENARD) {
						if(!main.utils.getJoueur(p).isUsedPower()) {
							if(args.length == 0) {
								p.sendMessage("§c/renard <joueur>");
							}else if(args.length == 1) {
								Player target = Bukkit.getPlayer(args[0]);
								if(target != null && main.utils.isInGame(p) && target != p && !main.utils.isDead(target)) {
									if(target.getLocation().distance(p.getLocation()) <= 10D) {
										if(main.utils.getRenardCounter() > 1) {
											main.utils.setRenardCounter(main.utils.getRenardCounter() -1);
										}else {
											main.utils.getJoueur(p).setUsedPower(true);
										}
										p.sendMessage("§aLe rôle de " + target.getName() +" est " + main.utils.getJoueur(target).getRole().getName() +".");
									}else {
										p.sendMessage("§cLe joueur doit se trouver à moins de 10 blocs de vous !");
									}
								}
							}
						}else {
							p.sendMessage("Vous avez déjà utilisé vos 3 coups !");
						}
					}
				}
			}else {
				p.sendMessage("§cLe mode Loup-Garous n'est pas activé !");
			}
		}
		return false;
	}

}
