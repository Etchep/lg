package fr.iban.lg.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.iban.lg.Main;
import fr.iban.lg.objects.Joueur;
import fr.iban.lg.objects.Role;
import fr.iban.lg.objects.Camp.CampType;

public class InfectCMD implements CommandExecutor {

	private Main main;
	
	public InfectCMD(Main main) {
		this.main = main;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player)sender;
			if(main.gameManager.isLgGame == true) {
				if(main.utils.isInGame(p)) {
					Joueur joueur = main.utils.getJoueur(p);
					if(joueur.getRole() == Role.INFECT_PERE_DES_LOUPS) {
						if(!main.utils.isDead(p)) {
							if(args.length == 0) {
								p.sendMessage("§c/infecte <Joueur>");
							}else if(args.length == 1) {
								if(!joueur.isUsedPower()) {
									Player target = Bukkit.getPlayer(args[0]);
									if(target != null) {
										if(main.utils.isInGame(target)) {
											if(main.deathByLG.contains(target.getUniqueId())) {
												main.utils.resurect(p);
												Joueur joueurtarget = main.utils.getJoueur(target);
												joueurtarget.setInfected(true);
												joueurtarget.setCamp(CampType.LOUP);
												target.sendMessage("§2§lVous avez été infecté ! Vous revivez au rang des Loup-Garous !");
												p.sendMessage("§2Vous avez infecté " + target.getName() +" !");
												joueur.setUsedPower(true);
											}else {
												p.sendMessage("§cCette personne n'est pas morte ou n'a pas été tuée par un LG.");
											}
										}else {
											p.sendMessage("§cCette personne ne joue pas.");
										}
									}else {
										p.sendMessage("§cCe joueur n'est pas en ligne !");
									}
								}else {
									p.sendMessage("§cVous avez déjà utilisé votre pouvoir !");
								}
							}
						}else {
							p.sendMessage("§cVous êtes mort !");
						}
					}else {
						p.sendMessage("§cVous n'êtes pas " + Role.INFECT_PERE_DES_LOUPS.getName() + " !");
					}
				}
			}else {
				p.sendMessage("§cLe mode Loup-Garous n'est pas activé !");
			}
		}
		return false;
	}

}
